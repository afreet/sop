# Hosting a react app with express server backend into a single application

Recently, I had to host an node express backend server and react frontend ~~server~~.
I thought, I have to host the backend server app seperately from the react app. So, I thought of doing the first step.
I made the binary executable using npm package known as '[pkg](https://www.npmjs.com/package/pkg)' by [vercel](https://github.com/vercel).

Pkg lets you pack the whole node project into a binary executable with all its dependencies and assets, if any.
It was quite easy.

- First point the main file of the server application in package.json as bin

  ```json
  "bin": "server.js"
  ```

- Then simply run pkg from the terminal

  ```console
   >> pkg .
  ```

  It was a clear shot, simple and easy.
  I made three binary, for every platform ie. windows, linux and mac.

After that, the second step was to try to host the react app, which was almost same as packing the server app. I didn't actually implemented that. 🤣

I directly jumped to the third step, trying to host the backend server and react frontend application into a single binary executable.

So, I tried to find out how to merge the frontend and backend server application into a single project.
I came to know about a blog which result amlost same to what I needed.
[How To Make create-react-app work with a Node Backend API](https://esausilva.com/2017/11/14/how-to-use-create-react-app-with-a-node-express-backend-api/) A very warm thanks to [Esau Silva](https://esausilva.com/)
The article is about how to make a react app in server itself, and host in Heroku.
So, I followed till it making the merged app only, and then I try to solve my half of the problem.

I will lay down simple steps that can be followed to achieve my problem's solution.

## Steps

_Prerequisites : You need to install nodejs and npm before starting the following steps._
_Also you can globally install the [create-react-app](https://create-react-app.dev/docs/getting-started/), [pkg](https://www.npmjs.com/package/pkg) and [nodemon](https://www.npmjs.com/package/nodemon) globally for ease._

- 1. Installing create-react-app, pkg and nodemon globally.

  - Installing create-react-app

  ```console
  >> npm install -g create-react-app
  ```

  - Installing pkg

  ```console
  >> npm install -g pkg
  ```

  - Installing nodemon

  ```console
  >> npm install -g nodemon
  ```

- 2. Make a root folder for your project. Let's say it "project-app". This will act as the main project folder which will have our server side code and inside it will have a sub folder for the frontend application.

- 3. In the terminal, open this folder and run the create-react-app(CRA) to generate the react application. I will give it a name "web-app"

  ```console
  >> npx create-react-app web-app
  ```

- 4. Initialize the package json using the init command in the root-folder, ~~not in the web-app folder~~.

  ```console
  >> npm init -y
  ```

_The below steps will be mostly done in the root-folder project unless specified for the web-app seperately._

- 5. Install the needed packages for server side like [express](https://www.npmjs.com/package/express), [morgan](https://www.npmjs.com/package/morgan), [nodemon](https://www.npmjs.com/package/nodemon) and [concurrently](https://www.npmjs.com/package/concurrently).

  ```console
  >> npm i express morgan
  >> npm i -D nodemon concurrently
  ```

- 6. Create some scripts in the package.json for launching the applications in the development

  ```json
  {
  ...
  "scripts": {
  "client": "cd [web-app] && npm run start",
  "server": "nodemon [server.js]",
  "dev": "concurrently --kill-others-on-fail \"npm run server\" \"npm run client\""
  "pack" : "pkg . -t [node12-win-x64]"
  }
  ...
  }
  ```

_The things between '[' ']' will change with your needs and name of folder and files._

- 7. Add the bin value in the package.json of the root folder for pkg to know which file in the entry point for the application.

```json
{
    ...
    "bin": "server.js"
    ...
}
```

- 8. Create a [server.js] in the root-folder as the starting file of the node server project and create your server application.

I will the port number of this express server '5000' for now.
Also all the routes will be made in mind that node server will act as api for the frontend so, all the routes can be like '/api/...' for seperation of concerns between frontend link which you will dope later on.

- 9. In the web-app folder, add a proxy value in the package json (web-app will also have a package.json)

  ```json
  {
      ...
      "proxy":"http://localhost:[5000]/",
      ...
  }
  ```

_This is the link of the server application which says, all the neccessary calls will be routed to this proxy. Change the port number according to your server application defined proxy._

- 10. In the web-app, do all the neccessary implementation of the frontend app as needed.

- 11. After all implementation in the frontend, build the application using below command in the web-app folder

  ```console
  >> npm run build
  ```

_This will generate a build folder in the web-app containing all the static stuffs generated using the react application code._

- 12. Create a pkg config in the package json of the root folder.

  ```json
  {
      ...
      "pkg":{
          "assets":[
              "[client-app]/build/**/*"
          ]
      }
  }
  ```

_This instruct the pkg to include all the neccessary static contents of the client application as the assets of the package itself. The build folder will contain all the react stuff and we are include all of it._

- 13. Add additional routing in the end to the index.html of the react application in the server routing, in order to route all the request other than api request to the client application.

  ```js
  app.use(express.static(path.join(__dirname, "[web-app]/build")));
  app.get("*", function (req, res) {
    res.sendFile(path.join(__dirname, "[web-app]/build", "index.html"));
  });
  ```

_The above lines will be added after the build has been made of the frontend._

- 14. Make your package when needed by running command in the root folder

  ```console
  >> npm run pack
  ```

_This will generate a binary file(s) according to your config and will be saved in the root folder itself._

## The things that need to be kept in mind

- I have only tested this steps for a simple project.
- JS was used as the language both in backend and frontend. I would love for typescript to work, it can be partially done, But hadn't implemented so can't say about that.
- Always trail and error of possibility you want to achieve in this type of project.
- I don't know how to set different port through the env in both server and frontend. Also I don't know, how to change them after packing the application.
- Please enlighten me for any things related to this.

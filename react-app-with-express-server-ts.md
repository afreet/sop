# Development of express server and react frontend using typescript and pack them into single binary executable.

Please refer to this [SOP](react-app-with-express-server-js.md) to get a clear picture and use case.

This SOP will be an extended version for the same using Typescript (Second point in the [last section](react-app-with-express-server-js.md#the-things-that-need-to-be-kept-in-mind) of above sop)

- So, for using typeScript in both frontend and backend, we will have this project structure

  ```console
  root-folder
  └───client-app (React project)
  │   └───src
  │   |   │   ts files
  │   └───build
  │   |   │   static files generated after react build
  │   └─package.json
  └───src (Server project)
  |   │   ts files
  └───dist
  |    │   generated js files using tsc
  └─package.json
  └─-- generated exe (executable binary)
  ```

- Changes in the scripts section for the root folder package.json

  ```json
  {
      ...
      "scripts": {
      "client": "cd [client-app] && npm run start",
      "startServer": "node dist/[index.js]",
      "devServer": "nodemon src/[index.ts]",
      "buildServer": "tsc -p .",
      "dev": "concurrently --kill-others-on-fail \"npm run devServer\" \"npm run client\"",
      "pack": "pkg . -t [node12-win-x64]"
      ...
    },
  }
  ```

- Another changes in the root folder package.json

  ```json
  {
      ...
      "main": "dist/index.js",
      "bin": "dist/index.js",
      ...
      "pkg": {
      "assets": [
        "[client-app]/build/**/*"
      ]
    }
  }
  ```

- In this [section](react-app-with-express-server-js.md#13-add-additional-routing-in-the-end-to-the-indexhtml-of-the-react-application-in-the-server-routing-in-order-to-route-all-the-request-other-than-api-request-to-the-client-application), there will be some changes

  ```js
  app.use(express.static(path.join(__dirname, "/../[client-app]/build")));
  app.get("*", function (_, res) {
    res.sendFile(path.join(__dirname, "/../[client-app]/build", "index.html"));
  });
  ```

- Proxy usage will be unchanged in the client app. _Just kill and start the react start from npm run start command, otherwise, proxy will not work. Wasted 30 minutes of my times :(_

- Example : Please find a sample project [here](https://gitlab.com/afreet/web-app-ts)

- Points to be noted

- I use nodemon + ts-node combo for development of express server using typescript. Please refer here [Node With Typescript](node-with-ts.md) for detail steps for that.

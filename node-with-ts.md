# Node with Typescript

Today, nodejs is a thing that everyone want to learn and applaud about. It is javascript runtime which helps us run javascript outside the browser.
It can be used to make light weight high performance server, which can resolute to make a good server option.

But, one thing, I personally don't like about javascript is, that it is **typeless** language, means it doesn't have basic strongly typed variables and objects, and many a things we usually see in a Object Oriented Programming world.

So, to eradicate this problem, a solution came up, that is using typescript and use it in writing code and then compiles back to javascript to serve it production.

By using typescript, mostly the runtime error which was occurring due to unhandled typos, undefined function and object name will be disappear.

But the setting up this development environment needs certain steps to follow.

I came across different method to do so, but this is the most easy method to do so. I have get to know about these steps from a great development tutor, that is,
[Brad Traversy](https://github.com/bradtraversy).
There is the tutorial I followed along and wrote these steps, if you want the steps only, copy and paste accordingly and start working.

## Steps

_Prerequisites : You need to install nodejs and npm before starting the following steps._

- Start with making a project folder where you want to make the environment, i will say it **_Test_**
- Start cmd and navigate into that folder, you just created.
- Firstly, we need to install typescript globally in order to have it supported everywhere.

  ```console
  >> npm i -g typescript
  ```

- After that, we want the typescript config file, **_tsconfig.json_** to make our typescript configuration.

  ```console
  >> tsc --init
  ```

  This will create the file where we can have all the typescript to javascript conversion rules, i.e. which ECMA Script standard the javascript will be converted into, or from source path, we get the _.ts_ files which will then transpiled into _.js_ and which output destination path it will be stored.

  - Some basic config statement to alter in **_tsconfig.json_** is :
    - "target": "es6"
    - "outDir": "./dist"
    - "rootDir": "./src"
    - "moduleResolution": "node"

- Now, make two folders in the "Test" folder, named **_dist_** and **_src_**. The _dist_ folder will have all the generated js files for the production, while _src_ will contain all the source ts files, where actual code will be written and also make a **_app.ts_** file in the _src_ folder, being the _app.ts_ will be the main ts file to start from.
- Now we initialize the npm to generate a **_package.json_**, where we have all the dependency and scripts listed to be used in our project.

  ```console
  >> npm init -y
  ```

- Now, we require **express** package to be installed to handle all our routes and stuffs, by running command

  ```console
  >> npm i express
  ```

- For our development related dependency and typescript types of node and express, we will install some dev-dependencies like [**typescript**](https://github.com/Microsoft/TypeScript), [**ts-node**](https://github.com/TypeStrong/ts-node), [**nodemon**](https://github.com/remy/nodemon), [**@types/node**](https://www.npmjs.com/package/@types/node), [**@types/express**](https://www.npmjs.com/package/@types/express)

  ```console
  >> npm i -D typescript ts-node nodemon @types/node @types/express
  ```

- Now, all the dependency and dev-dependency are over, time for some small scripts **dev**, so as to run our project in development phase, **build**, to transpile the typescript to javascript and **start** to lauch the projec in production mode.
  This is the snippet of the script object where we will make our scripts in _package.json_.

  ```json
  "scripts": { "start": "node dist/app.js", "dev": "nodemon src/app.ts", "build": "tsc -p ." }
  ```

- Now, the setup is ready to make some beautiful project of _nodejs_ with the help of typescript.
